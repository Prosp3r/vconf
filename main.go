package main

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:    1024,
	WriteBufferSize:   1024,
	EnableCompression: true,
}

func main() {

	//PRELOAD FROM DB FOR SPEED
	//users.Setusers()
	//users.Setsubscribers()

	//
	//DEFINE API END POINTS
	http.HandleFunc("/", servehome)
	http.HandleFunc("/assets/css/bootstrap.min.css", servebootstrapcss)
	http.HandleFunc("/assets/javascript/jquery.min.js", servejqueryjs)
	http.HandleFunc("/assets/javascript/bootstrap.min.js", servebootstrapjs)
	http.HandleFunc("/assets/css/bootstrap.min.css.map", parseMap)
	http.HandleFunc("/ws", socketConn)
	//END API ENDPOINTS DEFINITION

	http.ListenAndServe(":2000", nil)
	//log.Fatal(http.ListenAndServe(":2210", router))
}

//socketConn will initiate the web socket connection
func socketConn(w http.ResponseWriter, r *http.Request) {
	var conn, _ = upgrader.Upgrade(w, r, nil)
	go func(conn *websocket.Conn) {
		//read messages from client
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				conn.Close()
			}
		}
	}(conn)

	//write side
	go func(conn *websocket.Conn) {
		ch := time.Tick(5 * time.Second)
		for range ch {
			//SEND RESPONSE BACK AFTER DOING SOMETHING WITH THE DATA
			//conn.WriteMessage(myType, msg)
			//sending json object
			//type myjs struct{} //temporary declaration
			users.GetSubscribers()
			jsonObject := myjs{}
			conn.WriteJSON(jsonObject)
		}
	}(conn)
}

//servejqueryjs will return statis js files
func servejqueryjs(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/javascript/jquery.min.js")
}

//./assets/javascript/bootstrap.min.js
func servebootstrapjs(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/javascript/bootstrap.min.js")
}

//servebootstrap will serve the static bootstrap file
func servebootstrapcss(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/css/bootstrap.min.css")
}

func parseMap(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/css/bootstrap.min.css.map")
}

//servehome will return the static home page html file
func servehome(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}
