#  vCONF IS A SIMPLE VIDEO CONFERENCING APP WRITTEN IN GO.
 AUTHOR PROSPER ONOGBERIE

*  FEATURES

*  1 Grant audience/participants one-time access codes to meetings sent via AWS-SES 
*  2 Tightly Controlled Permissions 
*  3 Integrates Zoom API
*  4 Enable 1 to 1 and Many to Many live txt chat between participants.
*  5 Uses one zoom account for many participants.
*  6 Light server footprint. 1GB RAM droplet can handle 100 participants through efficient use of concurrency and Web-Sockets

* Bank level data encryption and credential security
* Messages can be persisted or set to self-destruct after a set period when the meeting ends
* Participants can choose to receive session transcripts/logs in their email even if they leave before the session is over.
* Spin up your own with one click on (http://samedayshop.com/vconf)

* CHALLENGE: This has to be done in 48 hours.
