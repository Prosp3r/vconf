package users

//User will hold the struct for users
type User struct {
	Userid      int64  `json:"userid"`
	Accesslevel int64  `json:"accesslevel"`
	Firstname   string `json:"firstname"`
	Lastname    string `json:"lastname"`
	Email       string `json:"email"`
	Phone       string `json:"phone"`
	Status      string `json:"status"`
}

//Attributes will hold abitrary attributes
type Attributes struct{}

//Userx is an instance of user struct
var Userx []User

//Setusers will return a struct of list
func Setusers() {}

//HELPER FUNCTIONS

//Create will add new user
func (user *User) Create()
