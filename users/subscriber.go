package users

//Subscribers will hold the structure of subscriber to a live chats session
type Subscribers struct {
	Userid         int64  `json:"userid"`
	Username       string `json:"username"`
	Meetingid      string `json:"meetingid"`
	Activelevel    string `json:"activelevel"`
	Accesscode     string `json:"accesscode"`
	Jointime       string `json:"jointime,omitempty"`
	Lastactivetime string `json:"lastactivetime,omitempty"`
}

//Subscribersx instance of subscribers
var Subscribersx []Subscribers

//Setsubscribers will populate the Subscribersx struct instance
func Setsubscribers() {}

//Getubscribers will return a struct list of current subscribers active and non active
func Getubscribers(meetingid string) []Subscribers {

	return nil
}
